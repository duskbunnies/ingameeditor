﻿using UnityEngine;
using System.Collections;

namespace Ironicnet.Framework.Level
{
    [RequireComponent(typeof(BoxCollider))]
    public class TileAutoSize : MonoBehaviour
    {
        BoxCollider collider;
        Transform graphicsTransform;
        // Use this for initialization
        void Start()
        {
            GetCollider();
        }

        private BoxCollider GetCollider()
        {
            collider = collider ?? GetComponent<BoxCollider>();
            return collider;
        }
        private Transform GetGraphicsTransform()
        {
            graphicsTransform = graphicsTransform ?? transform.FindChild("Graphics");
            return graphicsTransform;
        }

        // Update is called once per frame
        void Update()
        {

        }
        public virtual void ApplyTile(TileInstance instance)
        {
            LevelConfig config = LevelManager.Instance.LevelConfig;
            collider = GetCollider();
            collider.size = new Vector3(instance.Tile.Colspan * config.TileWidth, instance.Tile.Rowspan * config.TileHeight, 0.16f);
            collider.center = collider.size / 2;

            graphicsTransform = GetGraphicsTransform();
            graphicsTransform.localPosition = new Vector3(collider.center.x, collider.center.y, 0);
        }
    }

}