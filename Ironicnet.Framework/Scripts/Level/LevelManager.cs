﻿using System.Linq;
using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;

namespace Ironicnet.Framework.Level
{
    public class LevelManager : MonoBehaviour
    {
        public ResourceFile Resource;
        public string ResourceFile;
        public LevelConfig LevelConfig = new LevelConfig();
        public Dictionary<string, GameObject> Prefabs = new Dictionary<string, GameObject>();

        public Dictionary<string, Dictionary<string, Sprite>> Sprites = new Dictionary<string, Dictionary<string, Sprite>>();
        public Dictionary<ResourceTile, List<ResourceStyle>> Styles = new Dictionary<ResourceTile, List<ResourceStyle>>();

        protected string ResourcePath;

        public static LevelManager Instance
        {
            get;
            protected set;
        }

        public LevelManager()
        {
            Instance = this;
        }
        // Use this for initialization
        protected virtual void Start()
        {
            LoadResourceFile();
        }

        // Update is called once per frame
        protected virtual void Update()
        {

        }

        [ContextMenu("LoadResourceFile")]
        protected virtual void LoadResourceFile()
        {
            LoadResourceFile(GetResourcePath());
        }

        [ContextMenu("UpdateResourcePath")]
        protected virtual string GetResourcePath()
        {
            ResourcePath = Path.Combine(Application.dataPath, ResourceFile);
            return ResourcePath;
        }

        protected virtual void LoadResourceFile(string path)
        {
            ReadResourceFile(path);

            LoadStyles();

            LoadSprites();

            LoadPrefabsInstances();
        }

        protected virtual void ReadResourceFile(string path)
        {
            var serializer = new XmlSerializer(typeof(ResourceFile));
            using (var stream = new System.IO.FileStream(path, FileMode.Open))
            {
                Resource = serializer.Deserialize(stream) as ResourceFile;
                stream.Close();
            }
        }

        protected virtual void LoadStyles()
        {
            foreach (var styleGroup in Resource.Styles.GroupBy(s => s.Tile))
            {
                ResourceTile tile = Resource.Tiles.FirstOrDefault(t => t.Name == styleGroup.Key);
                if (tile != null)
                {
                    Styles.Add(tile, styleGroup.ToList());
                }
            }
        }

        protected virtual void LoadSprites()
        {
            foreach (var style in Resource.Styles)
            {
                LoadSpritesFromStyle(style);
            }
        }

        protected virtual void LoadPrefabsInstances()
        {

            foreach (var prefab in Resource.Prefabs)
            {
                var prefabInstance = Resources.Load(prefab.ResourcePath);
                if (prefabInstance != null)
                {
                    Prefabs.Add(prefab.Name, prefabInstance as GameObject);
                }
            }
        }

        protected virtual void LoadSpritesFromStyle(ResourceStyle style)
        {
            if (!Sprites.ContainsKey(style.ResourcePath))
            {
                var spriteDict = new Dictionary<string, Sprite>();
                var spritesInResource = Resources.LoadAll<Sprite>(style.ResourcePath);

                for (int i = 0; i < spritesInResource.Length; i++)
                {
                    Debug.Log(string.Format("{0}: {1}. Exists: {2}", i, spritesInResource[i].name, spriteDict.ContainsKey(spritesInResource[i].name)));
                    spriteDict.Add(spritesInResource[i].name, spritesInResource[i]);
                }
                Sprites.Add(style.ResourcePath, spriteDict);
            }
        }

        public virtual Sprite GetSprite(ResourceStyle style)
        {
            LoadSpritesFromStyle(style);

            if (Sprites.ContainsKey(style.ResourcePath) && Sprites[style.ResourcePath].ContainsKey(style.SpriteName))
            {
                return Sprites[style.ResourcePath][style.SpriteName];
            }
            else
            {
                return null;
            }

        }

        public virtual ResourceStyle GetStyle(ResourceTile tile, int styleIndex)
        {
            if (Styles.ContainsKey(tile) && styleIndex >= 0 && Styles[tile].Count > styleIndex)
            {
                return Styles[tile][styleIndex];
            }
            else
            {
                return null;
            }
        }

        public virtual List<ResourceStyle> GetStyles(ResourceTile CurrentSelectedTile)
        {
            if (Styles.ContainsKey(CurrentSelectedTile))
            {
                return Styles[CurrentSelectedTile];
            }
            else
            {
                return new List<ResourceStyle>();
            }
        }

        public virtual GameObject InstancePrefab(string prefabName)
        {
            if (Prefabs.ContainsKey(prefabName))
            {
                return GameObject.Instantiate(Prefabs[prefabName]);
            }
            else
            {
                return null;
            }
        }
    }
}
