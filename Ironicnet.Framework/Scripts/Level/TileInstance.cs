﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Ironicnet.Framework.Level
{
    [Serializable]
    public class TileInstance
    {
        public ResourceTile Tile;
        public ResourceStyle Style;
        public GameObject GameObject;
        public int TileX = -1;
        public int TileY = -1;
    }
}
