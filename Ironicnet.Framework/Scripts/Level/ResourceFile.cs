﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;

namespace Ironicnet.Framework.Level
{
    [XmlRoot("Resources")]
    [Serializable]
    public class ResourceFile
    {
        [XmlArray("Tiles")]
        [XmlArrayItem("Tile")]
        public List<ResourceTile> Tiles = new List<ResourceTile>();
        [XmlArray("Styles")]
        [XmlArrayItem("Style")]
        public List<ResourceStyle> Styles = new List<ResourceStyle>();
        [XmlArray("Prefabs")]
        [XmlArrayItem("Prefab")]
        public List<ResourcePrefab> Prefabs = new List<ResourcePrefab>();
    }
    [Serializable]
    public class ResourceTile
    {
        public ResourceTile ()
	{
            Rowspan = 1;
            Colspan = 1;
	}
        [XmlAttribute()]
        public string Name { get; set; }

        [XmlAttribute("Prefab")]
        public string PrefabName { get; set; }
        [XmlAttribute()]
        public int Rowspan { get; set; }
        [XmlAttribute()]
        public int Colspan{ get; set; }
        
    }
    [Serializable]
    public class ResourceStyle
    {
        [XmlAttribute()]
        public string Name { get; set; }
        [XmlAttribute()]
        public string Tile { get; set; }
        [XmlAttribute()]
        public string ResourcePath { get; set; }
        [XmlAttribute()]
        public string SpriteName { get; set; }

        [XmlAttribute()]
        public bool Spritesheet { get; set; }
    }
    [Serializable]
    public class ResourcePrefab
    {
        [XmlAttribute()]
        public string Name { get; set; }
        [XmlAttribute()]
        public string ResourcePath { get; set; }
    }
}
