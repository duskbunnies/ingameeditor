﻿using UnityEngine;
using System.Collections;

namespace Ironicnet.Framework.Level
{
    public class TileInstanceReference : MonoBehaviour 
    {

        public TileInstance Instance;
	    // Use this for initialization
	    void Start () {
	
	    }
	
	    // Update is called once per frame
	    void Update () {
	
	    }

        //This gets called when the tile is placed or the style changed
        public virtual void ApplyTile(TileInstance instance)
        {
            Instance = instance;
            SpriteRenderer[] renderers = transform.GetComponentsInChildren<SpriteRenderer>();
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].sprite = LevelManager.Instance.GetSprite(instance.Style);   
            }
        }
    }
}