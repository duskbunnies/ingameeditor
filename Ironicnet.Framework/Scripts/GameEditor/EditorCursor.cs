﻿using UnityEngine;
using System.Collections;
using Vectrosity;
using UnityEngine.UI;
using Ironicnet.Framework.Level;

namespace Ironicnet.Framework.GameEditor
{
    public class EditorCursor : MonoBehaviour
    {
        public Vector2 TilePosition;
        public ResourceTile SelectedTile;
        public ResourceStyle SelectedStyle;


        public Rect GridRect;
        public Material lineMaterial;
        public float lineWidth = 1;
        public LineType lineType = new LineType();
        protected VectorLine line;


        public bool Visible = true;

        public SpriteRenderer Renderer;

        void Awake()
        {
            GridRect = new Rect(0, 0, 0.16f, 0.16f);
            line = new VectorLine(gameObject.name, new Vector3[5], lineMaterial, lineWidth, lineType);

        }
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

            Render();
        }
        void Render()
        {
            line.active = Visible;
            Draw();
        }
        public Vector3[] points;
        [ContextMenu("Draw")]
        void Draw()
        {
            line.MakeRect(
                            new Vector3(transform.position.x + GridRect.x, transform.position.y + GridRect.y),
                            new Vector3(transform.position.x + GridRect.x + GridRect.width, transform.position.y + GridRect.y + GridRect.height)
                        );
            line.Draw();
            points = line.points3.ToArray();
        }


        public void SetPosition(Vector2 position)
        {
            if (TilePosition != position)
            {
                TilePosition = position;
                transform.position = new Vector3(TilePosition.x * LevelManager.Instance.LevelConfig.TileWidth, TilePosition.y * LevelManager.Instance.LevelConfig.TileHeight, 0);
            }
        }

        internal void SetTile(ResourceTile tile, ResourceStyle style)
        {

            SelectedTile = tile;
            SelectedStyle = style;
            GridRect.width = tile.Colspan * LevelManager.Instance.LevelConfig.TileWidth;
            GridRect.height = tile.Rowspan * LevelManager.Instance.LevelConfig.TileHeight;
            Renderer.transform.localPosition = new Vector3(GridRect.width / 2, GridRect.height / 2, 0);
            if (SelectedStyle != null)
            {
                Renderer.sprite = LevelManager.Instance.GetSprite(style);
                Renderer.enabled = true;
            }
            else
            {
                Renderer.sprite = null;
                Renderer.enabled = false;
            }
        }
    }

}