﻿using UnityEngine;
using System.Collections;
using System;
using Vectrosity;
using Ironicnet.Framework.Level;

namespace Ironicnet.Framework.GameEditor
{
    public class EditorManager : MonoBehaviour
    {

        public static EditorManager Instance
        {
            get;
            protected set;
        }
        public EditorManager()
        {
            Debug.Log("Setting EditorManager Instance", this);
            Instance = this;
        }
        private float inputTimer = 0;
        protected TileInstance[,] TileObjects;
        protected VectorLine LevelOutline;
        protected Rect LevelRect;

        protected string CurrentTilesCategory;
        protected int CurrentTileIndex;
        protected ResourceTile CurrentSelectedTile = null;
        protected int CurrentStyleIndex;
        protected ResourceStyle CurrentSelectedStyle = null;
        protected EditorInput input;

        public EditorCursor Cursor;

        public float inputDelay = 1f;

        public Material lineMaterial;
        public float lineWidth = 1;
        public LineType lineType = new LineType();
        public UnityEngine.UI.Text InfoText;

        // Use this for initialization
        protected virtual void Start()
        {
            LevelOutline = new VectorLine(this.name, new Vector3[8], lineMaterial, lineWidth, lineType);
            TileObjects = new TileInstance[LevelManager.Instance.LevelConfig.MapWidth, LevelManager.Instance.LevelConfig.MapHeight];
            Cursor.SetPosition(new Vector2(Mathf.CeilToInt(LevelManager.Instance.LevelConfig.MapWidth / 2), 10));
            LevelRect = new Rect(0, 0, LevelManager.Instance.LevelConfig.MapWidth * LevelManager.Instance.LevelConfig.TileWidth, LevelManager.Instance.LevelConfig.MapHeight * LevelManager.Instance.LevelConfig.TileHeight);
        }
        [Serializable]
        public class EditorInput
        {
            public float HorizontalAxis;
            public float VerticalAxis;
            public bool ButtonNextTileDown;
            public bool ButtonPreviousTileDown;
            public bool ButtonNextStyleDown;
            public bool ButtonPreviousStyleDown;
            public bool ButtonSelectDown;
            public bool ButtonCancelDown;
            public bool ButtonPlaceDown;
            public bool ButtonDeleteDown;
            public bool ButtonPlacePressed;
            public bool ButtonDeletePressed;
        }
        // Update is called once per frame
        protected virtual void Update()
        {

            input = GetInput();

            if (input.ButtonNextTileDown || input.ButtonPreviousTileDown)
            {
                CycleNextTile(input.ButtonNextTileDown);
            }
            if (input.ButtonNextStyleDown || input.ButtonPreviousStyleDown)
            {
                CycleStyle(input.ButtonNextStyleDown);
            }

            PositionCursor(input.HorizontalAxis, input.VerticalAxis);

            if (input.ButtonDeletePressed)
            {
                DeleteTileOnCursor();
            }
            else if (input.ButtonPlacePressed)
            {
                var currentObject = GetTileInstance((int)Cursor.TilePosition.x, (int)Cursor.TilePosition.y);
                Debug.Log(string.Format("Current Object: {0}", (currentObject != null ? (currentObject.Tile !=null ? currentObject.Tile.Name : "NO TILE" ): "NONE")));
                //If there is a different tile at that position, we delete it.
                if (currentObject != null && currentObject.Tile != CurrentSelectedTile)
                {
                    DeleteTileOnCursor();
                }

                //If there isn't any tile in that position, we place the tile there
                if (currentObject == null)
                {
                    currentObject = PlaceTileOnCursor(CurrentSelectedTile, CurrentSelectedStyle);
                }
                else
                {
                    //if there is a tile alrady there, we just change the tile and style to match the selection
                    currentObject.Tile = CurrentSelectedTile;
                    currentObject.Style = CurrentSelectedStyle;
                }
                //We let know the tile to apply the tile and style
                if (currentObject != null)
                {
                    currentObject.GameObject.SendMessage("ApplyTile", currentObject, SendMessageOptions.DontRequireReceiver);
                }
            }

            UpdateSelectionInfo();

            DrawLevelOutline();
        }

        protected virtual void DrawLevelOutline()
        {


            LevelOutline.MakeRect(
                            new Vector3(transform.position.x + LevelRect.x, transform.position.y + LevelRect.y),
                            new Vector3(transform.position.x + LevelRect.x + LevelRect.width, transform.position.y + LevelRect.y + LevelRect.height)
                        );
            LevelOutline.Draw();
        }

        protected virtual void UpdateSelectionInfo()
        {
            //We update the text of the selected tile / style
            if (CurrentSelectedTile != null && CurrentSelectedTile.Name != "")
            {
                InfoText.text = CurrentSelectedTile.Name;
                if (CurrentSelectedStyle != null && CurrentSelectedStyle.Name != "")
                    InfoText.text += ":" + CurrentSelectedStyle.Name;
            }
            else
            {
                InfoText.text = string.Empty;
            }
        }

        protected virtual TileInstance PlaceTileOnCursor(ResourceTile tile, ResourceStyle style)
        {
            int x = (int)Cursor.TilePosition.x;
            int y = (int)Cursor.TilePosition.y;
            return PlaceTileOnPosition(tile, style, x, y);
        }
        protected virtual TileInstance PlaceTileOnPosition(ResourceTile tile, ResourceStyle style, int tileX, int tileY)
        {
            Debug.Log(string.Format("PlaceTileOnPosition: TileX: {0}. TileY: {1}.", tileX, tileY));
            for (int y = 0; y < tile.Rowspan; y++)
            {
                for (int x = 0; x < tile.Colspan; x++)
                {
                    DeleteTileOnPosition(tileX + x, tileX + y);
                }
            }
            Debug.Log(string.Format("PlaceTileOnPosition: Instancing Prefab: {0} ({1} / {2})", tile.PrefabName, tile.Name, style!=null ?  style.Name : "NO STYLE"));
            TileInstance instance = new TileInstance();
            instance.Tile = tile;
            instance.Style = style;
            instance.TileX = tileX;
            instance.TileY = tileY;
            instance.GameObject = GetPrefab(tile, style, tileX, tileY);
            if (instance.GameObject == null)
            {
                Debug.Log(string.Format("PlaceTileOnPosition: Prefab not instantiated"));
                return null;
            }


            Debug.Log(string.Format("PlaceTileOnPosition: Filling Prefab: {0} ({1} / {2})", tile.PrefabName, tile.Name, style != null ? style.Name : "NO STYLE"));
            for (int y = 0; y < tile.Rowspan; y++)
            {
                for (int x = 0; x < tile.Colspan; x++)
                {
                    SetTileInstance(instance, tileX + x, tileY + y);
                }
            }
            return instance;
        }

        protected virtual void SetTileInstance(TileInstance instance, int targetX, int targetY)
        {
            if (targetX < TileObjects.GetUpperBound(0) && targetY < TileObjects.GetUpperBound(1))
            {
                TileObjects[targetX, targetY] = instance;
            }
        }
        protected virtual TileInstance GetTileInstance(int targetX, int targetY)
        {
            if (targetX < TileObjects.GetUpperBound(0) && targetY < TileObjects.GetUpperBound(1))
            {
                return TileObjects[targetX, targetY];
            }
            else
            {
                return null;
            }
        }

        protected virtual GameObject GetPrefab(ResourceTile tile, ResourceStyle style, int tileX, int tileY)
        {
            if (tile.PrefabName != null)
            {
                GameObject obj = LevelManager.Instance.InstancePrefab(tile.PrefabName);
                if (obj == null) return null;
                obj.name = tile.Name;
                obj.transform.position = new Vector3(tileX * LevelManager.Instance.LevelConfig.TileWidth, tileY * LevelManager.Instance.LevelConfig.TileHeight, obj.transform.position.z);
                return obj;
            }
            return null;
        }

        protected virtual void DeleteTileOnCursor()
        {
            int x = (int)Cursor.TilePosition.x;
            int y = (int)Cursor.TilePosition.y;
            DeleteTileOnPosition(x, y);
        }

        protected virtual void DeleteTileOnPosition(int tileX, int tileY)
        {
            Debug.Log(string.Format("DeleteTileOnPosition: TileX: {0} ({2}). TileY: {1} ({3}).", tileX, tileY, TileObjects.GetUpperBound(0), TileObjects.GetUpperBound(1)));
            var currentObject = GetTileInstance(tileX, tileY);
            if (currentObject != null)
            {
                Debug.Log(string.Format("DeleteTileOnPosition: Obj: {0}.", currentObject.Tile.Name), currentObject.GameObject);
                for (int y = 0; y < currentObject.Tile.Rowspan; y++)
                {
                    for (int x = 0; x < currentObject.Tile.Colspan; x++)
                    {
                        GameObject.Destroy(currentObject.GameObject);
                        SetTileInstance(null, currentObject.TileX + x, currentObject.TileY + y);
                    }
                }
            }
            else
            {
                Debug.Log("DeleteTileOnPosition: Empty.");
            }
        }

        protected virtual EditorInput GetInput()
        {
            EditorInput input = new EditorInput();
            input.HorizontalAxis = Input.GetAxis("EditorHorizontal");
            input.VerticalAxis = Input.GetAxis("EditorVertical");
            input.ButtonNextTileDown = Input.GetButtonDown("EditorNextTile");
            input.ButtonPreviousTileDown = Input.GetButtonDown("EditorPrevTile");
            input.ButtonNextStyleDown = Input.GetButtonDown("EditorNextStyle");
            input.ButtonPreviousStyleDown = Input.GetButtonDown("EditorPrevStyle");
            input.ButtonSelectDown = Input.GetButtonDown("EditorSelect");
            input.ButtonCancelDown = Input.GetButtonDown("EditorCancel");
            input.ButtonPlaceDown = Input.GetButtonDown("EditorPlace");
            input.ButtonPlacePressed = Input.GetButton("EditorPlace");
            input.ButtonDeleteDown = Input.GetButtonDown("EditorDelete");
            input.ButtonDeletePressed = Input.GetButton("EditorDelete");
            return input;
        }

        protected virtual void PositionCursor(float xAxis, float yAxis)
        {
            inputTimer -= Time.deltaTime;
            if (inputTimer <= 0)
            {
                Vector2 targetPosition = Cursor.TilePosition + new Vector2(xAxis == 0 ? 0 : (xAxis > 0 ? 1 : -1), yAxis == 0 ? 0 : (yAxis > 0 ? 1 : -1));
                if (targetPosition.x >= 0 && targetPosition.y >= 0 && targetPosition.x < LevelManager.Instance.LevelConfig.MapWidth && targetPosition.y < LevelManager.Instance.LevelConfig.MapHeight)
                {
                    inputTimer = inputDelay;
                    Cursor.SetPosition(targetPosition);

                }
            }
        }

        protected virtual void CycleNextTile(bool nextTile)
        {
            CurrentStyleIndex = 0;
            if (nextTile)
            {
                CurrentTileIndex++;
                if (CurrentTileIndex >= LevelManager.Instance.Resource.Tiles.Count)
                {
                    CurrentTileIndex = 0;
                }
            }
            else
            {
                CurrentTileIndex--;
                if (CurrentTileIndex < 0)
                {
                    CurrentTileIndex = LevelManager.Instance.Resource.Tiles.Count - 1;
                }
            }
            SetCursorCurrentTile();
            SetCursorCurrentStyle();
        }

        protected virtual void CycleStyle(bool nextStyle)
        {
            var tileStyles = LevelManager.Instance.GetStyles(CurrentSelectedTile);
            if (nextStyle)
            {
                CurrentStyleIndex++;
                if (CurrentStyleIndex >= tileStyles.Count)
                {
                    CurrentStyleIndex = 0;
                }
            }
            else
            {
                CurrentStyleIndex--;
                if (CurrentStyleIndex < 0)
                {
                    CurrentStyleIndex = tileStyles.Count - 1;
                }
            }
            SetCursorCurrentStyle();
        }

        protected virtual void SetCursorCurrentTile()
        {
            CurrentSelectedTile = LevelManager.Instance.Resource.Tiles[CurrentTileIndex];
        }

        protected virtual void SetCursorCurrentStyle()
        {
            CurrentSelectedStyle = LevelManager.Instance.GetStyle(CurrentSelectedTile, CurrentStyleIndex);
            Cursor.SetTile(CurrentSelectedTile, CurrentSelectedStyle);
        }
    }

}